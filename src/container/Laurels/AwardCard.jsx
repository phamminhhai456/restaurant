import React from "react";

export default function AwardCard(award) {
  return (
    <div className="app__laurels_awards-card">
      <img src={award.imgUrl} alt="awards" />
      <div className="app__laurels_awards-card_content">
        <p className="p__cormorant" style={{ color: "#DCCA87" }}>
          {award.title}abc
        </p>
        <p className="p__opensans">{award.subtitle}abc</p>
      </div>
    </div>
  );
}
